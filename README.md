# IDE

https://code.visualstudio.com/docs/languages/overview 
https://www.jetbrains.com/go/download/download-thanks.html?platform=mac

## Go UI dependency

go get github.com/andlabs/libui
go get github.com/andlabs/ui

## Vscode extensions 

- Go
- Go Docs
- Go Outliner

## Local dependencies

- Command + Shift + P ->  Go: Install/Update Tools -> select all
- Add proxy if install failed

```
go env -w GO111MODULE=on
go env -w GOPROXY=https://goproxy.io,direct
```

- Dependency list

```
jiodg45@jiodg45s-MacBook-Pro go % tree -L 2
.
├── bin
│   ├── dlv
│   ├── go-outline
│   ├── go-outliner
│   ├── gocode-gomod
│   ├── godef
│   ├── goimports
│   ├── gomodifytags
│   ├── gopkgs
│   ├── goplay
│   ├── gopls
│   ├── gotests
│   ├── impl
│   └── staticcheck
└── pkg
    ├── mod
    └── sumdb
```

## Demo 

![./demo.png](./demo.png)

## Support 

https://www.jianshu.com/p/456f072f58af